<?php
if(function_exists("apache_request_headers") && !empty(apache_request_headers())) exit("Forbidden");
// if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || empty($_POST['ui']) || !isset($_POST['path']) || !isset($_POST['sendcount'])) exit("Your are not authorized.");
// $id = $argv[1];
// $sendcount=$argv[2];
// $path=$argv[3];
$debug=false;
$paths2ignore=[]; //["no_op/"]


define("CLEAN",true);
require_once("../fwshelper.php");

if(file_exists(__DIR__ . '/vendor/autoload.php')) require __DIR__ . '/vendor/autoload.php';
else if (file_exists(__DIR__ . '/../vendor/autoload.php')) require __DIR__ . '/../vendor/autoload.php';
else require __DIR__ . '/../../vendor/autoload.php';

// $server="wss://echo.websocket.org:443";
$port=empty($config["port"])?8000:$config["port"];

function cleandata($wsfolder,$preftype="confirm",$action="checkdelete", $id="") {
	if(!is_array($preftype) && $preftype!=="_all_") $typefile=explode(",",$preftype);
	else $typefile=$preftype;
	$wsfiles=scandir($wsfolder);
	$ctime=microtime(true);
	$mintimediff=601;
	if($action=="delete") $mintimediff=1;
	foreach($wsfiles as $wsf)
	{
		$af=explode("_",$wsf);
		if(count($af)>1 && ($typefile==="_all_" || in_array($af[0],$typefile)) && (empty($id) || $id==$af[1])) {
			$wsfpath=$wsfolder."/".$wsf;
			$filetime=@filemtime($wsfpath);
			if($filetime!==false) {
				if($action=="checkdelete") {
					$cont=getNewLine($wsfpath);
					if($cont=="finished" || ($cont!==false && $ctime-$filetime>300)) {
						cleandata($wsfolder,"mess,send,confirm,handler","delete",$af[1]);
					}
				}
				if($action=="checktime") {
					if($filetime===false) {
						$mintimediff=0;
						if($GLOBALS["debug"]) echo "Unable to get modification time for the file $wsfpath. Returning $mintimediff\n";
					}
					else if($ctime-$filetime<$mintimediff) $mintimediff=$ctime-$filetime;
				}
				if($action=="delete") {
					$deleted=@unlink($wsfpath);
					if($deleted===false) {
						if($GLOBALS["debug"]) echo "Failed to delete file $wsfpath\n";
						$mintimediff=-1;
					}
				}
			}
		}
	}
	return $mintimediff;
}
function checkMessageToSend($conn,$csfile,&$sendcount,&$times,$id) {
	static $n=0;
	$n++;
	$mess=getNewLine($csfile,$sendcount);
	if(!empty($GLOBALS["log_loops"])) $written=file_put_contents("wslog.log", "- ".($n)." (".$GLOBALS["id"].") (path ".$GLOBALS["path"]."): ".date("Y-m-d H:i:s").' : sendcount='.$sendcount.', mess:`'.$mess.'`'.PHP_EOL, FILE_APPEND | LOCK_EX);
	if($mess!==false && $mess!=='' ) {
		if($mess ==  "!close!") {
			if($GLOBALS["debug"]) echo "Closing connection...\n";
			$conn->close();
		}
		else {
			if($GLOBALS["debug"]) echo "Sending: {$mess}\n\n";
			$conn->send($mess);
		}
		$sendcount++;
	}
	else {
		$conffile=$GLOBALS["folder"]."confirm_".$id;
		$conf=getNewLine($conffile);
		if(!empty($conf)) {
			if($conf!="finished") $times[$id]=strtotime($conf);
			else {
				if($GLOBALS["debug"]) echo "Closing connection...\n";
				$conn->close(1000,"closed:finished");
				return;
			}
		}
		$ctime=microtime(true);
		if($ctime-$times[$id]>$GLOBALS["confirm_delay"]) {
			$conn->close(1003,"error:confirmtimeout");
			return;
		}
		$cconfigcontents=file_get_contents("../config.json");
		if( $cconfigcontents !== false) $cconfig=json_decode($cconfigcontents,true);
		if(!empty($cconfig["emergency_stop"])) {
			$conn->close(1001,"closed:finished");
			//die("error:Sorry, the server is temporary switched off for maintenance/ Désolé, le serveur est temporairement en maintenance");
		}
	}
}

// $id = $_POST['ui'];
// $sendcount=$_POST['sendcount'];
// $path=$_POST['path'];
// $filesend="send_".$id;
// $filemess="mess_".$id;
$folder=empty($config["wsdata_path"])?"../wsdata":$config["wsdata_path"];
$folder.="/";
$handlers=[];
$connectors=[];
$r_connectors=[];
$sendcounts=[];
$times=[];
// $server="ws://localhost:".$port."/".$path;
function unset_all($id) {
	global $handlers,$connectors,$r_connectors,$sendcounts,$times;
	if(isset($handlers[$id])) unset($handlers[$id]);
	if(isset($connectors[$id])) unset($connectors[$id]);
	if(isset($r_connectors[$id])) unset($r_connectors[$id]);
	if(isset($sendcounts[$id])) unset($sendcounts[$id]);
	if(isset($times[$id])) unset($times[$id]);
}

$loop = \React\EventLoop\Factory::create();

$maintimer = $loop->addPeriodicTimer(0.2, function () use ($loop,$folder,&$handlers,&$connectors,&$sendcounts,&$times) {
	$wsfiles=scandir($folder);
	foreach($wsfiles as $wsf)
	{
		$af=explode("_",$wsf);
		$afcount=count($af);
		$wsfpath=$folder.$wsf;
		// $filetime=@filemtime($wsfpath);
		if($afcount>1 && isset($handlers[$af[1]]) && $handlers[$af[1]]<0) {
			$alldeleted=-1;
			if(cleandata($folder,"_all_","checktime",$af[1])>60) $alldeleted=cleandata($folder,"_all_","delete",$af[1]);
			if($alldeleted===1) unset_all($af[1]);
		}
		if($afcount>1 && $af[0]=="handler") {
			$handlertime=@filemtime($wsfpath);
			if($handlertime===false) continue;
			
			if(!isset($handlers[$af[1]])) {
				$handlers[$af[1]]=1;
			}
			// if($GLOBALS["debug"]) echo "handler ".$af[1].": ".$handlers[$af[1]].", " ;
		}
	}
	foreach($handlers as $hk=>$hv) {
		if($hv===1) {
			$hcontents=file_get_contents($folder."handler_".$hk);
			if($hcontents === false) continue;
			$ahcontents=json_decode($hcontents,true);
			if(in_array($ahcontents["path"],$GLOBALS["paths2ignore"])) {
				$handlers[$hk]=-2;
				continue;
			}
			if(!empty($ahcontents["ignore"]) && $ahcontents["ignore"]=='1') {
				$filecrtime=empty($ahcontents["creation_time"])?filemtime($folder."handler_".$hk):$ahcontents["creation_time"];
				$ctime=microtime(true);
				if($ctime-$filecrtime>300) $handlers[$hk]=-2;
				continue;
			}
			$connheaders=[];
			if(array_key_exists("Cookie",$ahcontents)) $connheaders["Cookie"]=$ahcontents["Cookie"];
			$server="ws://localhost:".$GLOBALS["port"]."/".$ahcontents["path"];
			if($GLOBALS["debug"]) echo "Connecting to $server\n";
			$r_connectors[$hk] = new \React\Socket\Connector($loop);
			$connectors[$hk] = new \Ratchet\Client\Connector($loop, $r_connectors[$hk]);
			$id=$hk;
			$sendcounts[$id]=0; $times[$id]=microtime(true);
			$filesend="send_".$id; $filemess="mess_".$id;
			$connectors[$hk]($server,[],$connheaders)->then(function(Ratchet\Client\WebSocket $conn) use($loop, $id, $folder, $filemess, $filesend, $ahcontents, &$handlers,  &$sendcounts, &$times) {
				$writemess=function($msg,$closing=false) use ($loop, $conn, $folder, $filemess, $id) {
					$cmfile=$folder.$filemess;
					$written=file_put_contents($cmfile, $msg.chr(30), FILE_APPEND | LOCK_EX);
					if(!$written) {
						if(!$closing) $conn->close(1002,'error:writing_file:'.$cmfile);
						// $loop->stop();
					}
					return $written;
				};
				$conn->on('message', function(\Ratchet\RFC6455\Messaging\MessageInterface $msg) use ($loop, $conn, $folder, $filemess, $ahcontents, $writemess, $id, &$sendcount) {
					if($GLOBALS["debug"]) echo "Received: {$msg}\n";
					$scriptfolder=$ahcontents["scriptfolder"];
					$repl_suff='/?'; if(!empty($ahcontents["replsuff"])) $repl_suff=$ahcontents["replsuff"];
					$msg=preg_replace('#\burl(\'|")?\s*:\s*(\'|")\s*/#','url$1: $2/'.$scriptfolder.$repl_suff,$msg);
					$msg=preg_replace('#_url(\'|")?\s*:\s*(\'|")\s*/#','_url$1: $2/'.$scriptfolder.$repl_suff,$msg);
					$writemess($msg);
				   // $conn->close();
				});

				$csfile=$folder.$filesend;
				$pertimer = $loop->addPeriodicTimer(0.24, function () use ($conn, $id, $csfile,&$sendcounts, &$times) {
					checkMessageToSend($conn,$csfile,$sendcounts[$id],$times,$id);
				});
				$conn->on('close', function($code = null, $reason = null) use ($loop, $csfile, $pertimer, $writemess, $id, &$handlers) {
					// $response = array('conn_closed'=>true, 'code' =>$code, 'reason' =>$reason);
					// $resp = json_encode($response);
					$resp='closed:normal';
					if(!empty($reason)) $resp=$reason;
					if(true || $reason !== 'error:timeout') {
						$loop->cancelTimer($pertimer);
						if(isset($GLOBALS["endtimer"])) $loop->cancelTimer($GLOBALS["endtimer"]);
					}
					// $filestodelete=[$csfile,$GLOBALS["folder"]."confirm_".$GLOBALS["id"]];
					$filestodelete=[$csfile];
					$deletewords=[]; $deletefilenames=[];
					foreach($filestodelete as $k=>$v) {
						$deletewords[$k]=(!file_exists($v))?"didn't exist":"deleted";
						$deletefilenames[$k]=($v==$csfile)?"sendfile":"confirmfile";
					}
					if($resp=='closed:normal' || $resp=='error:confirmtimeout') {				
						$filesdeleted=forcedeletefile($filestodelete);
					}
					$delfileinfo="";
					foreach($filestodelete as $k=>$v) {
						$deletedthisfile=!file_exists($filestodelete[$k]);
						$delfileinfo.=', '.$deletefilenames[$k].' `'.$filestodelete[$k].'` '.($deletedthisfile?$deletewords[$k]:'NOT DELETED');
					}
					if(!empty($GLOBALS["log_loops"])) $written=file_put_contents("wslog.log", "- connection closed (".$GLOBALS["id"]."): ".date("Y-m-d H:i:s").' : reason='.$reason.', resp:`'.$resp.'`'.$delfileinfo.PHP_EOL, FILE_APPEND | LOCK_EX);
					// echo $resp;
					if($GLOBALS["debug"]) echo "Closing: {$resp}\n";
					$writemess($resp,true);
					$handlers[$id]=-1;
					// $loop->stop();
				});

				checkMessageToSend($conn,$csfile,$sendcounts[$id],$times,$id);

				
				/*if(isset($GLOBALS["max_exec_time"]) && ini_get("max_execution_time")<$GLOBALS["max_exec_time"]) ini_set('max_execution_time', $GLOBALS["max_exec_time"]);
				$endtimerint=ini_get('max_execution_time')-2;
				if($endtimerint<=0) $endtimerint=110;
				// echo "max_execution_time:".ini_get('max_execution_time').", endtimerint:".$endtimerint."\n";

				$GLOBALS["endtimer"]=$loop->addTimer($endtimerint, function () use ($loop, $conn) {
					$loop->cancelTimer($GLOBALS["pertimer"]);
					$conn->close(1001, 'error:timeout');
					
				});*/
			}, function(\Exception $e) use ($loop, $folder, $filemess, $id) {
				$msg = "error:connection error for id $id: {$e->getMessage()}\n";
				$cmfile=$folder.$filemess;
				$written=file_put_contents($cmfile, $msg.chr(30), FILE_APPEND | LOCK_EX);
				echo date('Y-m-d H:i:s').': '.$msg;
				if(!$written) {
					echo date('Y-m-d H:i:s').': '."error writinig error message in file $cmfile!\n";
					// $loop->stop();
				}
			});
			$handlers[$hk]=2;
		}
	}
});
	


$loop->run();
	
?>