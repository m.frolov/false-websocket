<?php
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || empty($_POST['ui']) || empty($_POST['type'])) exit("Your are not authorized.");
$id = $_POST['ui'];

$configcontents=file_get_contents("config.json");
if( $configcontents !== false) $config=json_decode($configcontents,true);
if(!empty($config["emergency_stop"])) die("error:Sorry, the server is temporary switched off for maintenance/ Désolé, le serveur est temporairement en maintenance");

$file="confirm_".$id;
$folder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
$folder.="/";
$towrite=($_POST['type']!="finished")?date("Y-m-d H:i:s"):$_POST['type'];
$done=file_put_contents($folder.$file, $towrite.chr(30), LOCK_EX);
echo $done?"OK":"error:file_writing problem:$folder.$file";
?>