<?php
$ok=true;
if(!$ok) {
	header('Location: ../');
	exit;
}
	
$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
$acceptLang = ['fr', 'en']; 
$lang = in_array($lang, $acceptLang) ? $lang : 'en';

$fileforlinks="links.txt";
$message="";
$links=[];
if(file_exists($fileforlinks)) {
	$lconts=file_get_contents($fileforlinks);
	$links=explode(PHP_EOL,$lconts);
}
$id=1;
if(!empty($_REQUEST["linkid"]) && is_numeric($_REQUEST["linkid"])) $id=$_REQUEST["linkid"];

$n=0;
$toredirect="";
foreach($links as $clink) {
	$aclink=explode('://',$clink);
	// var_dump($aclink);
	if(substr(strtolower($aclink[0]),0,4)=="http" && !empty($aclink[1]))
	{
		$n++;
		if($n==$id) {
			$aqs=[];
			foreach($_GET as $getkey=>$getval) if($getkey!="linkid") $aqs[]="$getkey=$getval";
			$toredirect=$clink;
			if(count($aqs)>0) $toredirect.="?".implode("&",$aqs);
			break;
		}
	}
}
if(!empty($toredirect)) {
	header('Location: '.$toredirect);
	exit;
}
else {
	exit((($lang=="fr")?"Lien non trouvé":"Link not found"));
}