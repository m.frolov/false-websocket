<?php
$configcontents=file_get_contents("config.json");
if( $configcontents !== false) $config=json_decode($configcontents,true);
if(empty($_POST['key']) || $_POST['key'] != "FalseWebSocket" || empty($_POST['ui']) || !isset($_POST['data'])) exit("Your are not authorized.");
$id = $_POST['ui'];
$file="send_".$id;
$folder=empty($config["wsdata_path"])?"wsdata":$config["wsdata_path"];
$folder.="/";
$sent=file_put_contents($folder.$file, $_POST['data'].chr(30), FILE_APPEND | LOCK_EX);
if(trim($_POST['data'])=="!close!") file_put_contents($folder."mess_".$id, $_POST['data'].chr(30), FILE_APPEND | LOCK_EX);
if(!empty($_POST['unignore'])) {
	$hcontents=@file_get_contents($folder."handler_".$id);
	if(!empty($hcontents)) {
		$written=false;
		$ahcontents=@json_decode($hcontents,true);
		if($ahcontents !== null) {
			$ahcontents["ignore"]=0;
			$written=@file_put_contents($folder."handler_".$id,json_encode($ahcontents),LOCK_EX);
		}
		$sent=($sent && $written);
	}
}
echo $sent?"OK":"error:file_writing problem:$folder.$file";
?>